Quality counseling for couples, families individual adults and children. We treat a variety of problems ranging from anxiety to depression, PTSD, self-harm to self-esteem problems. Contact us for a free 15 minute phone consultation.

Address: 17451 Bastanchury Rd, Suite 204-33, Yorba Linda, CA 92886, USA

Phone: 714-438-4357

Website: http://www.newpaths.com

